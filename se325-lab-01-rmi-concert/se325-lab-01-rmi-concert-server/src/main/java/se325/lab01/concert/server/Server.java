package se325.lab01.concert.server;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import se325.lab01.concert.common.ConcertService;
import se325.lab01.concert.common.Config;


public class Server {
	
	public static void main(String[] args) {
        try {
        	ConcertService concertService = new ConcertServiceServant();
        	
        	Registry lookupService = LocateRegistry.createRegistry(Config.REGISTRY_PORT);
        	lookupService.rebind(Config.SERVICE_NAME, concertService);
        	
        	System.out.println("Server is up and running");
        	
        	 Keyboard.prompt("Press Enter to shutdown the server ");
             lookupService.unbind(Config.SERVICE_NAME);

        } catch (RemoteException e) {
            System.out.println("Unable to start or register proxy with the RMI Registry");
            e.printStackTrace();
        } catch (NotBoundException e) {
            System.out.println("Unable to remove proxy from the  RMI Registry");
        }
    }
}
