package se325.lab01.concert.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;

public class ConcertServiceServant  extends UnicastRemoteObject implements ConcertService{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id = 0L;
	private HashMap<Long, Concert> concerts = new HashMap<Long, Concert>();

	protected ConcertServiceServant() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Concert createConcert(Concert concert) throws RemoteException {
		concert.setId(id);
		concerts.put(concert.getId(), concert);
		id++;
		return concert;
	}

	@Override
	public Concert getConcert(Long id) throws RemoteException {
		Concert concert = concerts.get(id);
		return concert;
	}

	@Override
	public boolean updateConcert(Concert concert) throws RemoteException {
		if(concerts.get(concert.getId()) != null ) {
			concerts.replace(concert.getId(), concert);
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteConcert(Long id) throws RemoteException {
		if(concerts.get(id) != null) {
			concerts.remove(id);
			return true;
		}
		return false;
	}

	@Override
	public List<Concert> getAllConcerts() throws RemoteException {
		Collection<Concert> concertsList = concerts.values();
		ArrayList<Concert> concerts = new ArrayList<Concert>(concertsList);
		return concerts;
	}

	@Override
	public void clear() throws RemoteException {
		concerts.clear();
		
	}

}
