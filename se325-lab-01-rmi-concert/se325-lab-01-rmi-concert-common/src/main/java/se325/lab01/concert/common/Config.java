package se325.lab01.concert.common;

/**
 * Class with configuration settings for the application.
 */
public class Config {
    // Port number that the RMI registry will use.
    public static final int REGISTRY_PORT = 11000;

    // Name used to advertise/register the ConcertService service in the RMI Registry.
    public static final String SERVICE_NAME = "Concert-service";
}
