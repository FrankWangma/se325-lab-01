package se325.lab01.concert.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;
import se325.lab01.concert.common.Config;




public class Client {
	
	private static ConcertService proxy;

	@BeforeClass
	public static void getProxy() {
        try {
            // Instantiate a proxy object for the RMI Registry, expected to be
            // running on the local machine and on a specified port.
            Registry lookupService = LocateRegistry.getRegistry("localhost", Config.REGISTRY_PORT);

            // Retrieve a proxy object representing the ConcertService.
            proxy = (ConcertService) lookupService.lookup(Config.SERVICE_NAME);
        } catch (RemoteException e) {
            System.out.println("Unable to connect to the RMI Registry");
        } catch (NotBoundException e) {
            System.out.println("Unable to acquire a proxy for the Concert service");
        }
    }
	
	@Before
	public void clear() throws RemoteException {
		proxy.clear();
	}
	
	 @Test
	    public void testCreate() throws RemoteException {
		 try {
			 Concert concert1 = new Concert("Meme", LocalDateTime.now());
			 Concert concert2 = new Concert("Meme", LocalDateTime.now());
			 
			 Concert concertA = proxy.createConcert(concert1);
			 Concert concertB = proxy.createConcert(concert2);
			 
			 System.out.println(concertA.getId());
			 System.out.println(concertB.getId());
			 
			 List<Concert> concertsList = proxy.getAllConcerts();
			 
			 assertTrue(concertsList.contains(concertA));
			 assertTrue(concertsList.contains(concertB));
			 assertEquals(2, concertsList.size());
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 
	 }
}
